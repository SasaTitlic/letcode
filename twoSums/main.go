package main

import (
	"fmt"
)

func main() {
	res := twoSum(nums, 0)
	fmt.Printf("%+v\n", res)
}

var nums = []int{-3, 4, 3, 90}

func twoSum(nums []int, target int) []int {
	tmp := make(map[int]int)
	for i := range nums {
		index, ok := tmp[target-nums[i]]
		if ok {
			return []int{index, i}
		}
		tmp[nums[i]] = i
	}
	return []int{}
}
