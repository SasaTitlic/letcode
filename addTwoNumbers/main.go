package main

import "fmt"

func main() {

	tmp := &ListNode{Val: 1, Next: &ListNode{Val: 2, Next: &ListNode{Val: 3, Next: nil}}}
	out := getList(tmp)

	fmt.Printf("%+v\n", out)
	// res := addTwoNumbers(nums, 0)
}

type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	list1 := getList(l1)
	list2 := getList(l2)
	return &ListNode{}
}

func getList(list *ListNode) []int {
	var out []int
	tmp := list
	out = append(out, tmp.Val)
	for tmp.Next != nil {
		tmp = tmp.Next
		out = append(out, tmp.Val)
	}
	return out
}
